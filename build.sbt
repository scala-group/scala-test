import Dependencies._
import sbt._
import com.gilcloud.sbt.gitlab.GitlabPlugin

val vs_base = "0.4.0"
val vs_tag = sys.env.get("CI_COMMIT_BRANCH") match {
  case Some("master") => ""
  case _ => s"-SNAPSHOT"
}

logLevel := Level.Debug
ThisBuild / scalaVersion     := "2.13.2"
ThisBuild / version          := s"$vs_base$vs_tag"
ThisBuild / organization     := "net.benlist"
ThisBuild / organizationName := "benlist"

GitlabPlugin.autoImport.gitlabProjectId   :=  Some(20820514) // My Gitlab package-repo master project

lazy val scalaPubTest = (project in file("."))
  .settings(
    name := "scala-publish-test",
    resolvers += ("gitlab-maven-pull" at "https:/gitlab.com/groups/scala-group/-/packages"),
    credentials += Credentials(Path.userHome / ".sbt" / ".credentials.gitlab"),
    libraryDependencies += scalaTest % Test
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.

