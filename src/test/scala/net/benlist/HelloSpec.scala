package net.benlist

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import net.benlist.Model.{Point_2D,Point_3D}

class ModelSpec extends AnyFlatSpec with Matchers {
  "The Model object" should "add 2 Point_2D's" in {
    val p1 = Point_2D(1,1)
    val p2 = Point_2D(2,2)
    val expected = Point_2D(3,3)
    p1 add p2 shouldEqual expected
  }

  "The Model object" should "add 2 Point_3D's" in {
    val p1 = Point_3D(1,1,1)
    val p2 = Point_3D(2,2,2)
    val expected = Point_3D(3,3,3)
    p1 add p2 shouldEqual expected
  }
}
