package net.benlist

object Model {

  sealed trait Point[T <: Point[T]] {
    def add(r: T): T
  }

  case class Point_2D (x: Int, y: Int) extends Point[Point_2D] {
    override def add(r: Point_2D): Point_2D = Point_2D(x + r.x, y + r.y)
  }
  case class Point_3D (x: Int, y: Int, z: Int) extends Point[Point_3D] {
    override def add(r: Point_3D): Point_3D = Point_3D(x + r.x, y + r.y, z + r.z)
  }
}